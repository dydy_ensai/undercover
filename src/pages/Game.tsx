import {
  IonContent,
  IonIcon,
  IonPage,
  IonButton,
  IonItem,
  IonRange,
  IonLabel,
  IonHeader,
  IonToolbar,
  IonButtons,
  IonMenuButton,
  IonTitle,
  IonList,
  IonText,
  IonModal,
  IonInput
} from "@ionic/react";
import React, { useState } from "react";
import { useParams } from "react-router";
import "./Game.css";
import "./ChoixMot.tsx";

import { personAddOutline, personCircleOutline } from "ionicons/icons";

function shuffle(array: Array<string>) {
  array.sort(() => Math.random() - 0.5);
}

function getRandomInt(max: number) {
  return Math.floor(Math.random() * Math.floor(max));
}

function getDico() {
  return [
    ["NUAGE", "SOLEIL", "ORAGE", "PLUIE", "TEMPETE"],
    ["BANANE", "FRUIT", "POMME", "ORANGE"]
  ];
}

interface Joueur {
  name: string;
}



const Game: React.FC = () => {

  // console.log(useParams())
  const [nombreJoueur, setNombreJoueur] = React.useState(3);
  const [nombreCivil, setNombreCivil] = React.useState(2);
  const [nombreUndercover, setNombreUndercover] = React.useState(1);
  const [nombreMrWhite, setNombreMrWhite] = React.useState(0);
  const [motCivil, setMotCivil] = React.useState("");
  const [motUndercover, setMotUndercover] = React.useState("");


  //const [param, setParam] = React.useState(true);

  const [gamePhase, setGamePhase] = React.useState("configurationPhase");

  // index of the player eliminated at the last vote phase
  const [eliminatedPlayerIndex, setEliminatedPlayerIndex] = React.useState(-1);


  const { name } = useParams<{ name: string }>();

  const [tabJoueur, setTabJoueur]: Array<any> = React.useState([]);

  const updateComposition = (joueurs: number) => {

    const civil: number = Math.floor(joueurs / 2) + 1;
    const mrWhite: number = Math.floor((joueurs + 1) / 6);
    const undercover: number = joueurs - civil - mrWhite;
    setNombreJoueur(joueurs);
    setNombreCivil(civil);
    setNombreMrWhite(mrWhite);
    setNombreUndercover(undercover);

  };

  const updatePlayerName = (playerIndex: number, newName: string) => {

    let newTab = tabJoueur;
    newTab[playerIndex].name = newName;
    setTabJoueur(newTab);

  };

  const getPlayerIndexOfRole = (role: string) => {
    let res: Array<number> = [];
    tabJoueur.forEach((element: any, index: number) => {
      if (element.role === role) {
        res.push(index);
      }
    });
    return res;
  };


  const getAliveCompo = () => {
    let nbCivil = 0;
    let nbUndercover = 0;
    let nbMrWhite = 0;

    tabJoueur.forEach((element: any) => {
      if (element.isAlive) {
        if (element.role === "Civil") {
          nbCivil += 1;
        } else if (element.role === "Undercover") {
          nbUndercover += 1;
        } else if (element.role === "MrWhite") {
          nbMrWhite += 1;
        }
      }
    });

    return { nbCivil: nbCivil, nbUndercover: nbUndercover, nbMrWhite: nbMrWhite };
  }

  const ParamPage: React.FC = () => {
    return (
      <>
        <IonHeader>
          <IonLabel>
            <h1>Nombre de joueurs : {nombreJoueur}</h1>
          </IonLabel>
        </IonHeader>

        <IonRange
          value={nombreJoueur}
          snaps={true}
          min={3}
          max={20}
          step={1}
          color="secondary"
          id="curseurNombreJoueurs"
          //debounce={250}
          onIonChange={(e) => updateComposition(e.detail.value as number)}
        >
          <IonIcon slot="end" icon={personAddOutline} />
        </IonRange>

        <IonList inset={true} lines="inset" className="composition">

          <IonItem>
            <IonLabel>Civils</IonLabel>
            <IonText slot="end">{nombreCivil}</IonText>
          </IonItem>

          <IonItem>
            <IonLabel>Undercovers</IonLabel>
            <IonText slot="end">{nombreUndercover}</IonText>
          </IonItem>

          <IonItem>
            <IonLabel>Mr White</IonLabel>
            <IonText slot="end">{nombreMrWhite}</IonText>
          </IonItem>
        </IonList>

        <IonButton
          color="primary"
          expand="block"
          onClick={() => {
            initGame();
          }}
        >
          Commencer
        </IonButton>
      </>
    );
  };

  const getRoleList = () => {
    const roleList = Array(nombreCivil)
      .fill("Civil")
      .concat(Array(nombreUndercover).fill("Undercover"))
      .concat(Array(nombreMrWhite).fill("MrWhite"));
    return (roleList);
  };


  const initGame = () => {
    let allWords = getDico();
    let listWord = allWords[getRandomInt(allWords.length)];

    let motDesUndercover = listWord.splice(getRandomInt(listWord.length), 1)[0];
    let motDesCivils = listWord.splice(getRandomInt(listWord.length), 1)[0];


    let roleList: Array<string> = getRoleList();
    shuffle(roleList);

    let tabj: Array<any> = [];

    for (let i = 0; i < nombreJoueur; i++) {
      let playerKeyWord = "";
      if (roleList[i] === "Civil") {
        playerKeyWord = motDesCivils;
      } else if (roleList[i] === "Undercover") {
        playerKeyWord = motDesUndercover;
      } else { playerKeyWord = "Pas de mot" }

      tabj.push({
        name: "Joueur " + (i + 1),
        role: roleList[i],
        keyword: playerKeyWord,
        isAlive: true,
        wordSeen: false,
        buttonColor: "primary"
      });
    }
    console.log("Size tab joueur = " + tabj.length);
    setTabJoueur(tabj);
    setMotUndercover(motDesUndercover);
    setMotCivil(motDesCivils);
    setGamePhase("choixMotPhase");
  };

  const ChoixMotPage: React.FC = () => {
    const [etatPopup, setEtatPopup] = React.useState(-1);

    let currentName = "";
    let currentKeyword = "";

    if (etatPopup >= 0) {
      currentName = tabJoueur[etatPopup].name;
      tabJoueur[etatPopup].buttonColor = "warning";
      tabJoueur[etatPopup].wordSeen = true;
      if (tabJoueur[etatPopup].role === "MrWhite") {
        currentKeyword = "Vous êtes un Mr White !";
      } else {
        currentKeyword = "Voici votre mot : " + tabJoueur[etatPopup].keyword;
      }
    }

    return (
      <>
        <IonModal isOpen={etatPopup >= 0} >
          <IonHeader>
            <IonToolbar color="primary" mode="md">
              <IonTitle>Votre identité secrète</IonTitle>
            </IonToolbar>
          </IonHeader>

          <IonContent className="ion-padding">
            <h2>Votre pseudo actuel : {currentName}</h2>
            <IonInput placeholder="Ecrire pseudo ici" onIonChange={e => updatePlayerName(etatPopup, e.detail.value!)}></IonInput>
            <h2>{currentKeyword}</h2>
          </IonContent>

          <IonButton onClick={() => setEtatPopup(-1)}>Fermer</IonButton>
        </IonModal>

        {tabJoueur.map((object: any, index: number) => (
          <IonButton color={object.buttonColor} disabled={object.wordSeen} onClick={() => setEtatPopup(index)}>{object.name}<IonIcon icon={personCircleOutline}></IonIcon></IonButton>
        ))}
        <h2>-------------------------------</h2>
        <IonButton color="primary" disabled={false} onClick={() => setGamePhase("speakingPhase")}>Suivant</IonButton>

      </>
    );
  };


  const SpeakingPage: React.FC = () => {
    return (
      <>

        <p>Speaking Phase : Chaque joueur dit un mot chacun son tour.</p>
        <p>Liste des joueurs encore en vie:</p>
        <IonList>

          {tabJoueur.filter((object: any, index: number) => object.isAlive === true).map((object: any, index: number) => (
            <IonItem>
              <IonLabel>{object.name}</IonLabel>
            </IonItem>
          ))}


        </IonList>

        <h2>-------------------------------</h2>
        <p>Quand tout le monde a dit sont mot :</p>
        <IonButton color="primary" onClick={() => setGamePhase("votingPhase")}>Voter</IonButton>
      </>
    );
  };

  const VotingPage: React.FC = () => {

    const [chosenPlayerIndex, setChosenPlayerIndex] = React.useState(-1);

    const getPlayerButton = (object: any, indexPlayer: number) => {
      let buttonColor = 'primary';
      if (indexPlayer === chosenPlayerIndex) {
        buttonColor = 'warning';
      }
      return <IonButton color={buttonColor} onClick={() => setChosenPlayerIndex(indexPlayer)}>{object.name}<IonIcon icon={personCircleOutline}></IonIcon></IonButton>;
    }

    const onButtonClick = () => {
      setEliminatedPlayerIndex(chosenPlayerIndex);
      tabJoueur[chosenPlayerIndex].isAlive = false;
      if (tabJoueur[chosenPlayerIndex].role === "MrWhite") {
        setGamePhase("postVotePhaseMrWhite");
      }
      else { setGamePhase("postVotePhase"); }
    }

    return (
      <>
        <p>Tout le monde vote pour un joueur à éliminer.</p>
        <p>Presser le nom du joueur choisi :</p>

        {tabJoueur.filter((object: any, index: number) => object.isAlive === true).map((object: any, index: number) => (
          getPlayerButton(object, index)
        ))}
        <h2>-------------------------------</h2>
        <p>Quand vous avez choisi :</p>
        <IonButton color="primary" onClick={onButtonClick}>Suivant</IonButton>
      </>
    );
  };

  const GameOverPhaseMrWhite: React.FC = () => {

    return (
      <>

        <p>Game Over</p>
        <p>Liste des gagnants :</p>
        <IonList>

          {getPlayerIndexOfRole("MrWhite").map((playerIndex: any) => (
            <IonItem>
              <IonLabel>{tabJoueur[playerIndex].name} - {tabJoueur[playerIndex].role}</IonLabel>
            </IonItem>
          ))}
        </IonList>
        <IonButton color="primary" onClick={() => setGamePhase("configurationPhase")}>Nouvelle Partie</IonButton>
      </>
    );
  };


  const PostVotePhaseMrWhite: React.FC = () => {

    const [essaiMrWhite, setEssaiMrWhite] = React.useState("");
    const getComposition = () => {

      let compo = getAliveCompo();

      let res: Array<any> = []
      res.push(<IonItem> <IonLabel>{compo.nbCivil} Civil(s)</IonLabel> </IonItem>);
      res.push(<IonItem> <IonLabel>{compo.nbUndercover} Undercover(s)</IonLabel> </IonItem>);
      res.push(<IonItem> <IonLabel>{compo.nbMrWhite} MrWhite(s)</IonLabel> </IonItem>);
      return res;
    }

    const validationMrWhite = () => {

      //Si MrWhite devinne le mot:
      if (essaiMrWhite === motCivil) {
        setGamePhase("gameOverPhaseMrWhite")
      }
      //Si MrWhite ne devinne pas le mot:
      else {
        let compo = getAliveCompo();

        // if 1 seul civil, tt le mode gaggne sauf civil
        if (compo.nbCivil === 1) {
          setGamePhase("gameOverPhase");
        }
        // if que des undercover, undercover gagne
        else if (compo.nbCivil === 0 && compo.nbMrWhite === 0) {
          setGamePhase("gameOverPhase");
        }
        // if que des civil , civil gagne
        else if (compo.nbUndercover === 0 && compo.nbMrWhite === 0) {
          setGamePhase("gameOverPhase");
        }
        // if que des mister w, mister w gagnent
        else if (compo.nbCivil === 0 && compo.nbUndercover === 0) {
          setGamePhase("gameOverPhase");
        } else {
          setGamePhase("speakingPhase");
        }
      }

    }

    return (
      <>
        <p>Vous avez vote pour :</p>
        <p>{tabJoueur[eliminatedPlayerIndex].name}</p>
        <p>Son role etait :</p>
        <p>{tabJoueur[eliminatedPlayerIndex].role}</p>
        <h2>-------------------------------</h2>
        <p>Il reste :</p>
        <IonList>
          {getComposition()}
        </IonList>

        <IonInput placeholder="Ecrire mot ici" onIonChange={e => setEssaiMrWhite(e.detail.value!)}></IonInput>

        <IonButton color="primary" onClick={validationMrWhite}>Suivant</IonButton>
      </>
    );
  };

  const PostVotePage: React.FC = () => {

    const getComposition = () => {

      let compo = getAliveCompo();

      let res: Array<any> = []
      res.push(<IonItem> <IonLabel>{compo.nbCivil} Civil(s)</IonLabel> </IonItem>);
      res.push(<IonItem> <IonLabel>{compo.nbUndercover} Undercover(s)</IonLabel> </IonItem>);
      res.push(<IonItem> <IonLabel>{compo.nbMrWhite} MrWhite(s)</IonLabel> </IonItem>);
      return res;
    }

    const onButtonClick = () => {

      let compo = getAliveCompo();

      // if 1 seul civil, tt le mode gaggne sauf civil
      if (compo.nbCivil === 1) {
        setGamePhase("gameOverPhase");
      }
      // if que des undercover, undercover gagne
      else if (compo.nbCivil === 0 && compo.nbMrWhite === 0) {
        setGamePhase("gameOverPhase");
      }
      // if que des civil , civil gagne
      else if (compo.nbUndercover === 0 && compo.nbMrWhite === 0) {
        setGamePhase("gameOverPhase");
      }
      // if que des mister w, mister w gagnent
      else if (compo.nbCivil === 0 && compo.nbUndercover === 0) {
        setGamePhase("gameOverPhase");
      } else {
        setGamePhase("speakingPhase");
      }

    }

    return (
      <>
        <p>Vous avez vote pour :</p>
        <p>{tabJoueur[eliminatedPlayerIndex].name}</p>
        <p>Son role etait :</p>
        <p>{tabJoueur[eliminatedPlayerIndex].role}</p>
        <h2>-------------------------------</h2>
        <p>Il reste :</p>
        <IonList>
          {getComposition()}
        </IonList>

        <IonButton color="primary" onClick={onButtonClick}>Suivant</IonButton>
      </>
    );
  };


  const GameOverPage: React.FC = () => {


    const getWinnersIndex = () => {

      let compo = getAliveCompo();

      let winnersIndex: Array<number> = [];
      // if 1 suel civil, tt le mode gaggne sauf civil
      if (compo.nbCivil === 1) {
        winnersIndex = winnersIndex.concat(getPlayerIndexOfRole("Undercover"));
        winnersIndex = winnersIndex.concat(getPlayerIndexOfRole("MrWhite"));
      }
      // if que des undercover, undercover gagne
      else if (compo.nbCivil === 0 && compo.nbMrWhite === 0) {
        winnersIndex = winnersIndex.concat(getPlayerIndexOfRole("Undercover"));
      }
      // if que des civil , civil gagne
      else if (compo.nbUndercover === 0 && compo.nbMrWhite === 0) {
        winnersIndex = winnersIndex.concat(getPlayerIndexOfRole("Civil"));
      }
      // if que des mister w, mister w gagnent
      else if (compo.nbCivil === 0 && compo.nbUndercover === 0) {
        winnersIndex = winnersIndex.concat(getPlayerIndexOfRole("MrWhite"));
      }
      return winnersIndex;
    }



    return (
      <>

        <p>Game Over</p>
        <p>Liste des gagnants :</p>
        <IonList>

          {getWinnersIndex().map((playerIndex: any) => (
            <IonItem>
              <IonLabel>{tabJoueur[playerIndex].name} - {tabJoueur[playerIndex].role}</IonLabel>
            </IonItem>
          ))}
        </IonList>
        <IonButton color="primary" onClick={() => setGamePhase("configurationPhase")}>Nouvelle Partie</IonButton>
      </>
    );
  };


  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>{name}</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        {gamePhase === "configurationPhase" && <ParamPage />}
        {gamePhase === "choixMotPhase" && <ChoixMotPage />}
        {gamePhase === "speakingPhase" && <SpeakingPage />}
        {gamePhase === "votingPhase" && <VotingPage />}
        {gamePhase === "postVotePhase" && <PostVotePage />}
        {gamePhase === "gameOverPhase" && <GameOverPage />}
        {gamePhase === "postVotePhaseMrWhite" && <PostVotePhaseMrWhite />}
        {gamePhase === "gameOverPhaseMrWhite" && <GameOverPhaseMrWhite />}


      </IonContent>
    </IonPage>
  );
};

export default Game;
