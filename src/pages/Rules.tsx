import { IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar, IonSlides, IonSlide, IonImg, IonItem } from '@ionic/react';
import React from 'react';
import { useParams } from 'react-router';
//import './Page.css';

const Rules: React.FC = () => {
  console.log(useParams())
  const { name } = useParams<{ name: string; }>();

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>{name}</IonTitle>
        </IonToolbar>
      </IonHeader>



      <IonContent fullscreen class="ion-padding" scroll-y="false">
        <IonSlides>

          <IonSlide>
            <div>
              <img src="assets/img/page1.png" alt="slide1"></img>
              <h2>Regle page 1</h2>
              <p> <b>Il y a 3 rôles différents :</b> civil, undercover et Mr. White. Certains rôles vont avoir un mot qu'il faudra garder secret. Le téléphone sera passé entre les joueurs pour qu'ils découvrent leur rôles puis écrivent leur mots.</p>
            </div>
          </IonSlide>

          <IonSlide>
            <div>
              <img src="assets/img/civils.png" alt="slide2"></img>
              <h2>Les Civils</h2>
              <p>Ils reçoivent tous le même mot secret; leur objectif est de démasquer les undercovers & Mr White. Ils gagnent la partie lorsque les joueurs restant sont tous civils.</p>
            </div>
          </IonSlide>

          <IonSlide>
            <div>
              <img src="assets/img/undercovers.png" alt="slide3"></img>
              <h2>Les undercover</h2>
              <p>Ils reçoivent tous le même mot secret légèrement différent de celui des civils mais qui reste dans la même catégorie; leur objectif est de démasquer les civils & Mr White. Ils gagnent la partie lorsque les joueurs restant sont tous undercovers ou lorsqu'il ne reste qu'une seul civil.</p>
            </div>
          </IonSlide>

          <IonSlide>
            <div>
              <img src="assets/img/MrWhite.png" alt="slide4"></img>
              <h2>Mr White</h2>
              <p>Ils ne reçoivent pas de mot. Les Mr White gagnent s'il devinnent le mot des civils ou s'il ne reste qu'un civil.</p>
            </div>
          </IonSlide>

          <IonSlide>
            <div>
              <img src="assets/img/deroulement.png" alt="slide4"></img>
              <h2>Déroulement 1/2</h2>
              <p>Un à un, les joueurs lisent leur mot sur le téléphone (sauf Mr White).</p>
              <p>Le premier tour commence: les joueurs doivent dire un mot qui a rapport avec leur mot, chaque joueur après l'autre.</p>
              <p>Lorsque tout le monde a dit son mot, quelques minutes sont laissées pour voter contre l'unique personne à éliminer pendant ce tour.</p>
              <p>Le vote a lieu en même temps après un décompte.</p>
            </div>
          </IonSlide>

          <IonSlide>
            <div>
              <img src="assets/img/deroulement.png" alt="slide4"></img>
              <h2>Déroulement 2/2</h2>
              <p>La personne qui a le plus de votes meurt. Son identité est révélée.</p>
              <p>Si la personne morte est un Mr White, elle doit rentrer un mot sur le téléphone. Si ce mot est celui des civils, Mr White gagne.</p>
              <p>Sinon, le tour suivant commence. La partie se termine lorsqu'une équipe gagne.</p>
            </div>
          </IonSlide>

          <IonSlide>
            <div>
              <img src="assets/img/litiges.png" alt="slide4"></img>
              <h2>Les litiges</h2>
              <p>S'il reste 1 civil, les undercovers & Mr White gagnent. Seuls les civils ne gagnent pas.</p>
              <p>Si le vote produit une égalité, un bon vieux shi-fou-mi départagera les 2 partis.</p>
              <p></p>
            </div>
          </IonSlide>

          <IonSlide>
            <div>
              <img src="assets/img/pointsTechniques2.png" alt="slide4"></img>
              <h2>Aspects Techniques</h2>
              <p>La personne à jouer le premier tour n'est pas un Mr White.</p>
              <p>On ne peut pas voter pour soit.</p>
              <p>Il n'y a pas de mot composé (ex: arc-en-ciel, lait de soja)</p>
              <p>Il est interdit de dire un mot qui a déjà été dit.</p>
            </div>
          </IonSlide>

        </IonSlides>
      </IonContent>
    </IonPage>
  );
};

export default Rules;
