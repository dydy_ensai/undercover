import {
  IonContent, IonIcon, IonPage, IonButton, IonItem, IonRange, IonLabel, IonHeader,
  IonToolbar, IonButtons, IonMenuButton, IonTitle, IonList, IonText, IonListHeader, IonBackButton
} from '@ionic/react';
import React from 'react';
import { useParams } from 'react-router';
import Game from './Game';
import { personAddOutline } from 'ionicons/icons';

const ChoixMot: React.FC = () => {

  const { name } = useParams<{ name: string; }>();

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
            <IonBackButton />
          </IonButtons>
          <IonTitle>{name}</IonTitle>
        </IonToolbar>
      </IonHeader>


      <IonButton >
        Click
      </IonButton>

    </IonPage >
  );
};
export default ChoixMot;
