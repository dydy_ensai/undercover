import {
  IonContent,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenu,
  IonToggle,
  IonItemGroup,
  IonMenuToggle,
  IonNote,
} from '@ionic/react';

import React from 'react';
import { useLocation } from 'react-router-dom';
import { moonOutline, hammer, gameControllerOutline, gameControllerSharp, statsChartOutline, statsChartSharp, informationCircleOutline, informationCircleSharp } from 'ionicons/icons';


import './Menu.css';

interface AppPage {
  url: string;
  iosIcon: string;
  mdIcon: string;
  title: string;
}

const appPages: AppPage[] = [
  {
    title: 'Game',
    url: '/page/Game',
    iosIcon: gameControllerOutline,
    mdIcon: gameControllerSharp
  },
  {
    title: 'Score',
    url: '/Page',
    iosIcon: statsChartOutline,
    mdIcon: statsChartSharp
  },
  {
    title: 'Rules',
    url: '/page/Rules',
    iosIcon: informationCircleOutline,
    mdIcon: informationCircleSharp
  },
  {
    title: 'A propos',
    url: '/page/A propos',
    iosIcon: informationCircleOutline,
    mdIcon: informationCircleSharp
  }
];

const Menu: React.FC = () => {
  const location = useLocation();
  return (
    <IonMenu contentId="main" type="overlay">
      <IonContent>
        <IonList id="inbox-list">
          <IonListHeader>Undercover</IonListHeader>
          <IonNote>On met du texte ?</IonNote>
          {appPages.map((appPage, index) => {
            return (
              <IonMenuToggle key={index} autoHide={false}>
                <IonItem className={location.pathname === appPage.url ? 'selected' : ''} routerLink={appPage.url} routerDirection="none" lines="none" detail={false}>
                  <IonIcon slot="start" icon={appPage.iosIcon} />
                  <IonLabel>{appPage.title}</IonLabel>
                </IonItem>
              </IonMenuToggle>
            );
          })}
        </IonList>
        <IonList lines="none">
          <IonListHeader>Settings</IonListHeader>
          <IonItemGroup>
            <IonItem button >
              <IonIcon slot="start" icon={hammer} />
             Show Tutorial
            </IonItem>
            <IonItem>
              <IonIcon slot="start" icon={moonOutline}></IonIcon>
              <IonLabel>Dark Mode</IonLabel>
              <IonToggle />
            </IonItem>
          </IonItemGroup>
        </IonList>
      </IonContent>
    </IonMenu>
  );
};

export default Menu;
